/* eslint-disable no-console */
import axios from 'axios'

axios.interceptors.request.use(
    config => {
        console.log('axios request 请求拦截器')
        console.log('axios request 请求拦截器参数')
        // console.log(config)
        config.headers['Ann-Token'] = 'ANN-JWT-TOKEN'
        return config
    },
    error => {
        console.log('axios request 拦截器错误')
        return Promise.reject(error)
    }
)
export default axios